package com.example.okaad_000.lab1;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class Lab1Activity1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lab1_activity1);

        final Button button1 = findViewById(R.id.B1);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText text1 = findViewById(R.id.T1);
                Intent intent = new Intent(Lab1Activity1.this, Lab1Activity2.class);
                Bundle bundle = new Bundle();
                bundle.putString("TEXT1", text1.getText().toString());
                intent.putExtras(bundle);
                Lab1Activity1.this.startActivity(intent);
            }
        });

        // Populate the spinner with values
        Spinner spinner = findViewById(R.id.L1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_choices, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        // Fetch from preferences if set
        SharedPreferences preference = getSharedPreferences("PREFERENCE", 0);
        String defaultSpinnerValue = preference.getString("selected", getResources().getString(R.string.spin1));
        spinner.setSelection(adapter.getPosition(defaultSpinnerValue));

        class ItemSelectedListener implements AdapterView.OnItemSelectedListener {

            boolean firstSelection = true;

            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (!firstSelection) {
                    String selected = parent.getItemAtPosition(pos).toString();

                    SharedPreferences preference = getSharedPreferences("PREFERENCE", 0);
                    SharedPreferences.Editor editor = preference.edit();
                    editor.putString("selected", selected);
                    editor.commit();
                }
                else {
                    firstSelection = false;
                }
            }

            public void onNothingSelected(AdapterView parent) {
                // Do nothing.
            }
        }

        spinner.setOnItemSelectedListener(new ItemSelectedListener());
    }
}
