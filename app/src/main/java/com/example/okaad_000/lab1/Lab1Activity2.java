package com.example.okaad_000.lab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Lab1Activity2 extends AppCompatActivity {

    static final int TEXT_A3 = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lab1_activity2);

        final Button button2 = findViewById(R.id.B2);

        Bundle bundle = getIntent().getExtras();

        String value = "";

        if (bundle != null)
        {
            TextView text2 = findViewById(R.id.T2);

            value = bundle.getString("TEXT1");

            text2.setText(getResources().getString(R.string.A2T2) + value);
        }

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Lab1Activity2.this, Lab1Activity3.class);
                Lab1Activity2.this.startActivityForResult(intent, TEXT_A3);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final TextView text3 = findViewById(R.id.T3);

        // Check which request we're responding to
        if (requestCode == TEXT_A3) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String message = data.getStringExtra("TEXT");
                text3.setText(getResources().getString(R.string.A2T3) + message);
            }
        }
    }
}
