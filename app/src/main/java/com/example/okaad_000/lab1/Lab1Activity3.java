package com.example.okaad_000.lab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Lab1Activity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lab1_activity3);

        final Button button3 = findViewById(R.id.B3);

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText text4 = findViewById(R.id.T4);
                String message = text4.getText().toString();
                Intent intent = new Intent();
                intent.putExtra("TEXT", message);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
